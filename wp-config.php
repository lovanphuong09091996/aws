<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<5_iS85IKC{#)0!d2j??Iu6Owvl/Ga5??W@-o1U^76K`fl)WNoBccPG7a[,CoA:r');
define('SECURE_AUTH_KEY',  'H3(v){A9;Y>n]!Z$~?g+,quqCtONl1h!@,B${:5OT0cDtnBP8j$[6Q,+-Hr8I :,');
define('LOGGED_IN_KEY',    '.#=_b0)r5OILRt%OrYx{W71KRNBqw+UL(K9xjgZW<OiKA-p=e=|XX}+_kb[av~tC');
define('NONCE_KEY',        'me=/`i#VGdfvF6Gs#P;~#U7h^8OS~M=(:4>ye3@dmHP`v{?b*kc1I e<%?fkF`WG');
define('AUTH_SALT',        'W1~NpctXnZVpWN/)CkfA-NO:wWg^&7J=[]g$:HimSz9xKq)[):ZqyCqwzPTMm]i(');
define('SECURE_AUTH_SALT', ',rGmT8WTae@n[,,U^ &_.R/T#f7WuQ0BM^cYy VN4M=[2yhHJ-FS>/hR2AtwxKa#');
define('LOGGED_IN_SALT',   's-Qj*nSXUk#fg9^Z{>l#x7NrvFJ)Zb;oTC<<JMcqei4Hl,vT5ZZo<_SFIG%Etbr7');
define('NONCE_SALT',       '{tlAOs9GV,-(5@baY,bv3=^jp+SlcINzXoni|%#K44Un3XSK`H^`X/#Wk95rHz<R');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
