<section data-test-id="home_certification_section" class="awardedBy-section_1ns">
    <div class="awardedBy-section__container_1zY">
        <div class="content_3oC">
            <h2 class="content__title_3bV">Awards</h2>
            <h3 class="content__description_1gS">Better Sleep, Naturally</h3>
            <div class="content__logos_2M3">
                <div class="content__logos-item_28r"><img class="content__award-image_1fj" lr=""
                        lr-loader-triggers="screen"
                        lr-loader-actions="image:https://media.awarasleep.com/awara/awara-best-latex-mattress-v1.png?width=150&amp;auto=webp"
                        lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="awara-best-latex-mattress"
                        src="https://media.awarasleep.com/awara/awara-best-latex-mattress-v1.png?width=150&amp;auto=webp">
                    <h3 class="content__award-title_2l_">Tuck Award</h3>
                    <p class="content__award-text_1uj">Editor’s Pick<br> Best Latex Mattress<br> 2019
                    </p>
                </div>
                <div class="content__logos-item_28r"><img class="content__award-image_1fj" lr=""
                        lr-loader-triggers="screen"
                        lr-loader-actions="image:https://media.awarasleep.com/awara/awara-best-natural-organic-mattress-v1.png?width=100&amp;auto=webp"
                        lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                        alt="awara-best-natural-organic-mattress"
                        src="https://media.awarasleep.com/awara/awara-best-natural-organic-mattress-v1.png?width=100&amp;auto=webp">
                    <h3 class="content__award-title_2l_">Slumber Yard</h3>
                    <p class="content__award-text_1uj">Best Natural Organic Mattress<br> 2019</p>
                </div>
                <div class="content__logos-item_28r"><img class="content__award-image_1fj" lr=""
                        lr-loader-triggers="screen"
                        lr-loader-actions="image:https://media.awarasleep.com/awara/awara-best-organic-eco-friendly-mattress-v1.png?width=150&amp;auto=webp"
                        lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                        alt="awara-best-organic-eco-friendly-mattress"
                        src="https://media.awarasleep.com/awara/awara-best-organic-eco-friendly-mattress-v1.png?width=150&amp;auto=webp">
                    <h3 class="content__award-title_2l_">Tuck Award</h3>
                    <p class="content__award-text_1uj">Editor’s Pick<br> Best Eco-Friendly Mattress<br> 2019
                    </p>
                </div>
                <div class="content__logos-item_28r"><img class="content__award-image_1fj" lr=""
                        lr-loader-triggers="screen"
                        lr-loader-actions="image:https://media.awarasleep.com/awara/awara-top-rated-organic-mattress-v1.png?width=112&amp;auto=webp"
                        lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                        alt="awara-top-rated-organic-mattress"
                        src="https://media.awarasleep.com/awara/awara-top-rated-organic-mattress-v1.png?width=112&amp;auto=webp">
                    <h3 class="content__award-title_2l_">Mattress Advisor</h3>
                    <p class="content__award-text_1uj">Best Organic Mattress<br> 2019</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section data-test-id="home_layers_3d_section" class="home__layers-mattress-3d-section_15_ LayersMattress3DSection_2Zw">
    <h2 class="LayersMattress3DSection__title_2Cr">Natural &amp; Organic Materials That Speak for Themselves
    </h2><span class="LayersMattress3DSection__description_3TW">The Awara Mattress
        features a 9-inch layer of premium, individually-wrapped pocket coils to help ensure full-body
        support and proper spinal alignment while you sleep. In addition, the coils help evenly
        distribute weight and reduce motion transfer for undisturbed rest. Combined with the contouring
        latex layer, the pocket coil system also gives Awara that touch of bounce our customers
        love.</span>
    <div class="section3D_14r">
        <div class="section3D__image-wrapper_Th0">
            <div class="animation__fade-in-up section3D__image_1B9 reveal-render3d-conditional_3bm animation__fade-in-up--end"
                lr="" lr-loader-triggers="screen:+300"
                lr-loader-actions="imageLg:https://media.awarasleep.com/awara/awara-mattress-layers-v1.png?width=935&amp;auto=webp|imageMd:https://media.awarasleep.com/awara/awara-mattress-layers-better-v1.png?width=668&amp;auto=webp|imageXs:https://media.awarasleep.com/awara/awara-mattress-layers-best-v1.png?width=500&amp;auto=webp"
                lr-revealer-triggers="middle:+100"
                lr-revealer-actions="addClasses:reveal-render3d-conditional_3bm:animation__fade-in-up--end" role="img"
                aria-label="awara-natural-organic-materials-mattress"></div>
        </div>
        <div class="section3D__item-list_2J7">
            <div class="item-with-icon_2M0"><svg xmlns="http://www.w3.org/2000/svg" height="27" width="25"
                    viewBox="0 0 25 26" fill="none" class="item__icon_Pxr"><svg>
                        <mask id="path-1-inside-1" fill="white">
                            <path
                                d="M25.1851 13.8294C25.1851 23.3175 18.565 26.7958 12.595 26.7958C9.25462 26.7958 6.05102 25.3843 3.689 22.8718C1.32697 20.3594 0 16.9518 0 13.3986C0 9.84547 1.32697 6.43785 3.689 3.9254C6.05102 1.41295 9.25462 0.0014649 12.595 0.00146484C18.595 0.00146484 23.4651 3.79883 24.7151 9.78739">
                            </path>
                        </mask>
                        <path
                            d="M12.595 26.7958V28.2958V26.7958ZM0 13.3986H1.5H0ZM12.595 0.00146484V-1.49854V0.00146484ZM23.6851 13.8294C23.6851 18.1925 22.1805 20.9958 20.1533 22.7311C18.085 24.5017 15.3119 25.2958 12.595 25.2958V28.2958C15.8481 28.2958 19.3701 27.3507 22.1043 25.0101C24.8796 22.6343 26.6851 18.9544 26.6851 13.8294H23.6851ZM12.595 25.2958C9.68338 25.2958 6.87075 24.0663 4.78187 21.8444L2.59613 23.8993C5.2313 26.7023 8.82586 28.2958 12.595 28.2958L12.595 25.2958ZM4.78187 21.8444C2.69004 19.6193 1.5 16.5828 1.5 13.3986H-1.5C-1.5 17.3207 -0.0360946 21.0994 2.59613 23.8993L4.78187 21.8444ZM1.5 13.3986C1.5 10.2144 2.69004 7.17789 4.78187 4.95284L2.59613 2.89796C-0.0360947 5.69781 -1.5 9.4765 -1.5 13.3986H1.5ZM4.78187 4.95284C6.87075 2.73093 9.68338 1.50146 12.595 1.50146L12.595 -1.49854C8.82587 -1.49854 5.2313 0.0949657 2.59613 2.89796L4.78187 4.95284ZM12.595 1.50146C17.9057 1.50146 22.1461 4.82092 23.2467 10.0939L26.1834 9.4809C24.784 2.77673 19.2844 -1.49854 12.595 -1.49854V1.50146Z"
                            fill="#4C3043" mask="url(#path-1-inside-1)"></path>
                        <path
                            d="M23.9349 11.2229L21.4749 15.7542C21.4482 15.8012 21.4338 15.8548 21.4331 15.9096C21.4324 15.9645 21.4455 16.0185 21.471 16.0662C21.4964 16.1139 21.5334 16.1534 21.578 16.1809C21.6227 16.2083 21.6734 16.2226 21.7249 16.2223H26.645C26.6965 16.2226 26.7472 16.2083 26.7919 16.1809C26.8365 16.1534 26.8734 16.1139 26.8989 16.0662C26.9244 16.0185 26.9374 15.9645 26.9367 15.9096C26.936 15.8548 26.9216 15.8012 26.895 15.7542L24.4349 11.2229C24.4093 11.1766 24.3728 11.1382 24.3289 11.1115C24.2851 11.0848 24.2355 11.0708 24.1849 11.0708C24.1344 11.0708 24.0848 11.0848 24.0409 11.1115C23.9971 11.1382 23.9605 11.1766 23.9349 11.2229Z"
                            fill="#4C3043"></path>
                    </svg></svg>
                <div class="item_a_S">
                    <h4 class="item__title_3Jg">Breathable Organic Materials</h4>
                    <p class="item__description_3r7">Plush Euro Top cover made with soft New Zealand wool &amp;
                        organic cotton</p>
                </div>
            </div>
            <div class="item-with-icon_2M0"><svg xmlns="http://www.w3.org/2000/svg" height="27" width="25"
                    viewBox="0 0 25 26" fill="none" class="item__icon_Pxr"><svg>
                        <path
                            d="M25 11.8077C25 11.8077 22.41 9.79987 12.5 11.8077C2.33 13.8156 0 11.8077 0 11.8077V3.30448C0 2.6841 0.245481 2.08913 0.682441 1.65045C1.1194 1.21178 1.71205 0.965332 2.33 0.965332L22.67 0.965332C23.288 0.965332 23.8806 1.21178 24.3176 1.65045C24.7545 2.08913 25 2.6841 25 3.30448V11.8077Z"
                            fill="#4C3043"></path>
                        <path
                            d="M25 23.7143C25.0013 24.0223 24.942 24.3275 24.8255 24.6125C24.709 24.8975 24.5376 25.1565 24.3211 25.3748C24.1046 25.5931 23.8473 25.7663 23.564 25.8844C23.2806 26.0026 22.9768 26.0635 22.67 26.0635H2.33002C2.0232 26.0635 1.7194 26.0026 1.43605 25.8844C1.15271 25.7663 0.895409 25.5931 0.678917 25.3748C0.462425 25.1565 0.291007 24.8975 0.174504 24.6125C0.058 24.3275 -0.00129538 24.0223 2.14612e-05 23.7143V15.211C2.14612e-05 15.211 1.91002 16.9277 12.5 15.211C22.86 13.5244 25 15.211 25 15.211V23.7143Z"
                            fill="#4C3043"></path>
                    </svg></svg>
                <div class="item_a_S">
                    <h4 class="item__title_3Jg">4” of Cooling Natural Dunlop Latex Foam</h4>
                    <p class="item__description_3r7">Sourced from real rubber trees - 2” thicker than standard
                        hybrid mattresses</p>
                </div>
            </div>
            <div class="item-with-icon_2M0"><svg xmlns="http://www.w3.org/2000/svg" height="27" width="25"
                    viewBox="0 0 25 26" fill="none" class="item__icon_Pxr"><svg>
                        <path
                            d="M17.34 0.469804L18.9 0.0498047H1.80998C1.71461 0.0518558 1.62015 0.0687239 1.52998 0.0998046C1.0416 0.324607 0.650883 0.718278 0.429764 1.20833C0.208644 1.69838 0.172011 2.25182 0.326609 2.76674C0.481208 3.28166 0.816629 3.7234 1.27111 4.01061C1.7256 4.29782 2.26854 4.41117 2.79998 4.32979C4.07998 4.00979 5.35998 3.6698 6.62998 3.3298L8.62998 2.7998L13 1.6398L17.34 0.469804Z"
                            fill="#4C3043"></path>
                        <path
                            d="M21.29 5.80965C21.7818 5.70116 22.2209 5.42571 22.5326 5.03012C22.8444 4.63454 23.0095 4.14322 23 3.63966C22.9975 3.29702 22.9167 2.95948 22.7639 2.6528C22.6111 2.34611 22.3902 2.07838 22.1182 1.87002C21.8462 1.66167 21.5302 1.51819 21.1943 1.45054C20.8584 1.38289 20.5114 1.39285 20.18 1.47966L17.18 2.26966L14.83 2.89966L11.19 3.89966L8.31998 4.65965L6.18998 5.21965C4.67998 5.61965 3.18998 6.01965 1.65998 6.43965C1.2986 6.52591 0.966568 6.7065 0.697776 6.96299C0.428985 7.21949 0.233054 7.5427 0.129977 7.89965C-0.00174097 8.27382 -0.0237144 8.67786 0.0666479 9.06411C0.15701 9.45037 0.35594 9.80273 0.639977 10.0796C0.92826 10.4043 1.30618 10.6365 1.72614 10.7468C2.14611 10.8571 2.58932 10.8407 2.99998 10.6996L7.69998 9.43964L12.41 8.18964L21.32 5.80965H21.29Z"
                            fill="#4C3043"></path>
                        <path
                            d="M22.9999 9.99966C22.9937 10.5064 22.8224 10.9973 22.512 11.3979C22.2016 11.7985 21.7691 12.087 21.2799 12.2197L20.0899 12.5297L14.2299 14.0997C10.4633 15.1063 6.68994 16.1063 2.90994 17.0996C2.51283 17.2255 2.08799 17.2349 1.68569 17.1268C1.28339 17.0187 0.92049 16.7976 0.639943 16.4896C0.341244 16.2033 0.134276 15.8347 0.0452649 15.4306C-0.0437467 15.0265 -0.0107954 14.605 0.139943 14.2197C0.271053 13.8442 0.497683 13.5093 0.797522 13.2481C1.09736 12.9868 1.46007 12.8081 1.84994 12.7297L5.18994 11.8497L8.53994 10.9997L12.9999 9.77966L17.4999 8.58967L20.0699 7.90967C20.6374 7.7505 21.2446 7.81929 21.762 8.10137C22.2795 8.38345 22.6663 8.8565 22.8399 9.41967C22.8919 9.586 22.9287 9.7567 22.9499 9.92966L22.9999 9.99966Z"
                            fill="#4C3043"></path>
                        <path
                            d="M20.73 14.1899C21.2992 14.1925 21.847 14.407 22.2667 14.7915C22.6864 15.1761 22.9478 15.7031 23 16.2699C23.0412 16.8095 22.8857 17.3458 22.5621 17.7797C22.2386 18.2135 21.7689 18.5155 21.24 18.6299L15.75 20.0899L10.26 21.5499L6.61998 22.5499L2.99998 23.4899C2.58782 23.6253 2.14483 23.6359 1.72669 23.5202C1.30856 23.4046 0.933953 23.1679 0.649978 22.8399C0.355408 22.5486 0.155049 22.1755 0.0748223 21.769C-0.00540387 21.3625 0.038195 20.9413 0.199978 20.5599C0.321806 20.21 0.530815 19.8969 0.807269 19.6502C1.08372 19.4035 1.4185 19.2313 1.77998 19.1499L7.64998 17.5899L10.58 16.8099L15.36 15.5499L20.14 14.2799L20.6 14.2099L20.73 14.1899Z"
                            fill="#4C3043"></path>
                        <path
                            d="M5.86 24.48L7.68 24L13.49 22.44L19.3 20.89H19.47C19.77 20.81 20.09 20.73 20.4 20.67C20.9531 20.5908 21.5158 20.7221 21.9766 21.038C22.4375 21.3539 22.763 21.8313 22.8888 22.3757C23.0145 22.9201 22.9313 23.4918 22.6556 23.9778C22.3799 24.4638 21.9318 24.8286 21.4 25C21.3139 25.0145 21.2261 25.0145 21.14 25H4L5.86 24.48Z"
                            fill="#4C3043"></path>
                    </svg></svg>
                <div class="item_a_S">
                    <h4 class="item__title_3Jg">A Thick 9” Contouring Support Core of Premium Pocketed Coils
                    </h4>
                    <p class="item__description_3r7">Individually-wrapped coils provide optimal pressure relief
                        and spinal support</p>
                </div>
            </div>
            <div class="item-with-icon_2M0"><svg xmlns="http://www.w3.org/2000/svg" height="27" width="25"
                    viewBox="0 0 25 26" fill="none" class="item__icon_Pxr"><svg>
                        <g clip-path="url(#clip0)">
                            <path
                                d="M12.9999 1.14707C14.4246 1.14748 15.8251 1.51595 17.0655 2.21676C18.3059 2.91757 19.3443 3.92696 20.0799 5.14708V3.07708C20.0799 2.32651 19.7818 1.60669 19.251 1.07596C18.7203 0.545231 18.0005 0.24707 17.2499 0.24707H8.73991C7.98935 0.24707 7.26953 0.545231 6.7388 1.07596C6.20807 1.60669 5.90991 2.32651 5.90991 3.07708V5.15708C6.64517 3.93365 7.68457 2.92135 8.92698 2.21865C10.1694 1.51596 11.5725 1.14679 12.9999 1.14707Z"
                                fill="#4C3043"></path>
                            <path
                                d="M25.0901 13.2472C25.0897 14.672 24.7212 16.0724 24.0204 17.3128C23.3196 18.5533 22.3102 19.5916 21.0901 20.3273H23.1801C23.9289 20.3246 24.6462 20.0253 25.1747 19.4948C25.7033 18.9644 26.0001 18.2461 26.0001 17.4973V8.98723C26.0001 8.2384 25.7033 7.52009 25.1747 6.98964C24.6462 6.4592 23.9289 6.15987 23.1801 6.15723H21.0801C22.3035 6.89249 23.3158 7.93188 24.0185 9.1743C24.7212 10.4167 25.0904 11.8199 25.0901 13.2472Z"
                                fill="#4C3043"></path>
                            <path
                                d="M12.9999 25.3372C11.5735 25.3385 10.171 24.9708 8.92867 24.2699C7.68633 23.569 6.6464 22.5587 5.90991 21.3371V23.4272C5.91256 24.176 6.21189 24.8932 6.74233 25.4218C7.27277 25.9504 7.99108 26.2472 8.73991 26.2472H17.2499C17.9987 26.2472 18.7171 25.9504 19.2475 25.4218C19.7779 24.8932 20.0773 24.176 20.0799 23.4272V21.3271C19.3455 22.5491 18.3077 23.5605 17.0672 24.2631C15.8267 24.9657 14.4256 25.3357 12.9999 25.3372Z"
                                fill="#4C3043"></path>
                            <path
                                d="M0.9 13.2472C0.898685 11.8208 1.26633 10.4183 1.96723 9.17599C2.66812 7.93365 3.67843 6.89372 4.9 6.15723H2.83C2.07944 6.15723 1.35962 6.45539 0.828888 6.98612C0.29816 7.51685 0 8.23667 0 8.98723L0 17.4973C0 18.2478 0.29816 18.9676 0.828888 19.4984C1.35962 20.0291 2.07944 20.3273 2.83 20.3273H4.91C3.68802 19.5929 2.67665 18.5551 1.97404 17.3145C1.27143 16.074 0.901447 14.6729 0.9 13.2472Z"
                                fill="#4C3043"></path>
                        </g>
                        <defs>
                            <clipPath id="clip0">
                                <rect width="25" height="25.0001" fill="white" transform="translate(0 0.24707)">
                                </rect>
                            </clipPath>
                        </defs>
                    </svg></svg>
                <div class="item_a_S">
                    <h4 class="item__title_3Jg">Soft Base Layer w/ 4 Reinforced Handles</h4>
                    <p class="item__description_3r7">Beautiful cotton/poly base adds a luxe touch to your
                        bedroom decor</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section data-test-id="home_our_product_section" class="">
    <div class="proof-grid_UC0 big-image-right_1Qq">
        <div class="proof-grid__row_2b_">
            <div class="proof-grid__img-col_1cv">
                <div class="proof-grid__img-wrap_1vM">
                    <div class="animation__fade-in-up proof-grid__img_2hF animation__fade-in-up--end" lr=""
                        lr-loader-triggers="screen:+300"
                        lr-loader-actions="imageLg:https://media.awarasleep.com/awara/natural-organic-latex-mattress-desktop-v1.jpg?width=1000&amp;auto=webp|imageMd:https://media.awarasleep.com/awara/natural-organic-latex-mattress-tablet-v1.jpg?width=768&amp;auto=webp|imageXs:https://media.awarasleep.com/awara/natural-organic-latex-mattress-mobile-v1.jpg?width=768&amp;auto=webp"
                        lr-revealer-triggers="middle:+100"
                        lr-revealer-actions="setBg|addClasses:animation__fade-in-up--end" role="img"
                        aria-label="Natural &amp; Organic Latex Mattress"
                        style="background-image: url(&quot;https://media.awarasleep.com/awara/natural-organic-latex-mattress-desktop-v1.jpg?width=1000&amp;auto=webp&quot;);">
                    </div>
                </div>
            </div>
            <div class="proof-grid__content-col_3hD">
                <div class="proof-grid__content-wrap_2-W">
                    <div class="content_1_P">
                        <h3 class="vertical-headline vertical-headline--animated content__headline_2jX vertical-headline--animated-end"
                            lr="" lr-revealer-triggers="screen:-100"
                            lr-revealer-actions="addClasses:vertical-headline--animated-end">Our Product
                        </h3>
                        <h2 class="content__title_3Wj">The Results of Natural &amp; Organic Latex Speak
                            <br> for Themselves</h2>
                        <div class="content__desc_YRI">MADE WITH A LAYER OF RAINFOREST ALLIANCE-CERTIFIED DUNLOP
                            LATEX, BORN FROM NATURE AND LOVINGLY CRAFTED TO BRING YOU YOUR BEST SLEEP.</div><a
                            class="btn-animated  content__button_TEv" href="/mattress"><span btn-title="SHOP MATTRESS"
                                class="btn-animated__content"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>